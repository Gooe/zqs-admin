/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : localhost:3306
 Source Schema         : zqs_admin

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 06/01/2022 14:20:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for zqs_admin
-- ----------------------------
DROP TABLE IF EXISTS `zqs_admin`;
CREATE TABLE `zqs_admin` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `username` varchar(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(50) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(32) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(30) NOT NULL DEFAULT '' COMMENT '密码盐',
  `headimg` int(10) NOT NULL DEFAULT '0' COMMENT '头像',
  `email` varchar(100) NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录时间',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `token` varchar(59) NOT NULL DEFAULT '' COMMENT 'Session标识',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of zqs_admin
-- ----------------------------
BEGIN;
INSERT INTO `zqs_admin` VALUES (1, 'admin', '超级管理A', '22ab08e556ca8e6287d85dfd8d1d4e16', 'L3edBl', 21, 'admin@admin.com', 0, 1641062630, 1492186163, 1641062630, '4e0882b1-fd36-46be-89c2-63196091668e', 1);
INSERT INTO `zqs_admin` VALUES (4, 'admin1', 'admin1', 'f6b7a9603e6dacb05436260bcdaf7b33', '8q9JbK', 0, 'b@c.com2', 0, 1617727442, 1586180353, 1618078294, 'abaa187e-37df-480b-b4da-e272c90e0906', 1);
INSERT INTO `zqs_admin` VALUES (5, 't1', 't1', '044dd47a7fb0dd71bbcc7feb0a21c2f0', 'Ir0Veh', 0, '11@qq.com', 0, 1586191210, 1586180889, 1586340073, '960aea62-a69f-4fe8-b2be-d02a6b00020a', 1);
COMMIT;

-- ----------------------------
-- Table structure for zqs_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `zqs_admin_log`;
CREATE TABLE `zqs_admin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `username` varchar(30) NOT NULL DEFAULT '' COMMENT '管理员名字',
  `url` varchar(100) NOT NULL DEFAULT '' COMMENT '操作页面',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '日志标题',
  `content` text NOT NULL COMMENT '内容',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) NOT NULL DEFAULT '' COMMENT 'User-Agent',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  PRIMARY KEY (`id`),
  KEY `name` (`username`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员日志表';

-- ----------------------------
-- Records of zqs_admin_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for zqs_article
-- ----------------------------
DROP TABLE IF EXISTS `zqs_article`;
CREATE TABLE `zqs_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cover` int(11) NOT NULL DEFAULT '0' COMMENT '封面',
  `cate_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(150) CHARACTER SET utf8 NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read` int(11) NOT NULL DEFAULT '0' COMMENT '阅读次数',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文章表';

-- ----------------------------
-- Records of zqs_article
-- ----------------------------
BEGIN;
INSERT INTO `zqs_article` VALUES (19, 1, 2, '测试一篇文章', '&lt;p&gt;帮助！！！！！！！！！！！！！！！！！&lt;/p&gt;\n&lt;p&gt;&lt;img src=&quot;/storage/uploads/image/20200410\\171bfeb319505617e5937b3c61f20981.gif&quot; alt=&quot;&quot; width=&quot;177&quot; height=&quot;177&quot; /&gt;&lt;/p&gt;', 0, 1586943665, 1586947159, 1, 0);
INSERT INTO `zqs_article` VALUES (20, 19, 10, '文章2', '<p>1111???ddddddddddddddddddddddd<img src=\"/storage/uploads/image/20210126/5e97675502aee45135d52425be6eba1e.png\" alt=\"\" width=\"180\" height=\"180\" /></p>', 0, 1586959973, 1611643387, 1, 0);
COMMIT;

-- ----------------------------
-- Table structure for zqs_article_cate
-- ----------------------------
DROP TABLE IF EXISTS `zqs_article_cate`;
CREATE TABLE `zqs_article_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cover` int(11) NOT NULL DEFAULT '0' COMMENT '封面',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父类',
  `name` varchar(20) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `show_index` tinyint(1) NOT NULL DEFAULT '0' COMMENT '首页显示',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='文章分类';

-- ----------------------------
-- Records of zqs_article_cate
-- ----------------------------
BEGIN;
INSERT INTO `zqs_article_cate` VALUES (2, 11, 0, '新手入门', 4, 1, 1576423429, 1576424912, 1);
INSERT INTO `zqs_article_cate` VALUES (10, 19, 2, '小分类', 0, 0, 1586956312, 1586956312, 1);
INSERT INTO `zqs_article_cate` VALUES (11, 1, 0, '分类二', 5, 0, 1586960732, 1586960732, 1);
COMMIT;

-- ----------------------------
-- Table structure for zqs_attachment
-- ----------------------------
DROP TABLE IF EXISTS `zqs_attachment`;
CREATE TABLE `zqs_attachment` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `cate_id` int(11) NOT NULL DEFAULT '0' COMMENT '分类id',
  `uid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '物理路径',
  `imagewidth` varchar(30) NOT NULL DEFAULT '' COMMENT '宽度',
  `imageheight` varchar(30) NOT NULL DEFAULT '' COMMENT '宽度',
  `imagetype` varchar(30) NOT NULL DEFAULT '' COMMENT '图片类型',
  `imageframes` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片帧数',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `mimetype` varchar(255) NOT NULL DEFAULT '' COMMENT 'mime类型',
  `extparam` varchar(255) NOT NULL DEFAULT '' COMMENT '透传数据',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建日期',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `upload_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `sha1` varchar(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `storage` varchar(100) NOT NULL DEFAULT 'local' COMMENT '存储位置,local,qiniu',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `sha1` (`sha1`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='附件表';

-- ----------------------------
-- Records of zqs_attachment
-- ----------------------------
BEGIN;
INSERT INTO `zqs_attachment` VALUES (33, 0, 0, 'img.jpg', '/storage/uploads/image/20201215/22526752761aba93da44989efb285415.jpg', '750', '520', 'jpg', 0, 201847, 'image/jpeg', '', 1607961894, 1607961894, 1607961894, '23adb457c4e0a09d461f529878798404a7a24ac6', 'public', 1);
INSERT INTO `zqs_attachment` VALUES (34, 0, 0, 'user1.jpeg', '/storage/uploads/image/20201215/c05dfc59da3e224ce9757998ae9c6588.jpeg', '400', '400', 'jpeg', 0, 52277, 'image/jpeg', '', 1607962131, 1607962131, 1607962131, '8624f0af5af3542b7dcc70dab6d67bc895d02136', 'public', 1);
INSERT INTO `zqs_attachment` VALUES (37, 0, 0, 'user1.jpg', '/uploads/image/20201215/e912b6013bc3c852ec0f769372b10449.jpg', '500', '495', 'jpg', 0, 27728, 'image/jpeg', '', 1607962823, 1607962823, 1607962823, 'd2b07452970722013182ba64e1b06ee2338ebd18', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (38, 0, 0, 'user2.jpg', '/uploads/image/20201215/baf2dc3c733d9b518480bd97debd6b33.jpg', '420', '420', 'jpg', 0, 13881, 'image/jpeg', '', 1607962836, 1607962836, 1607962836, '04317e69d04ed0c39f63386685cfd128b13fe1e0', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (39, 0, 0, 'user3.jpeg', '/uploads/image/20201215/baf2dc3c733d9b518480bd97debd6b33.jpeg', '700', '700', 'jpeg', 0, 169362, 'image/jpeg', '', 1607962836, 1607962836, 1607962836, 'fcfe45ecf5481fe811855705082ef863c21dda1b', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (40, 0, 0, 'tx01.jpg', '/uploads/image/20201215/58e0c9e6b0dbc8092ae9a6c0a944014f.jpg', '200', '200', 'jpg', 0, 5119, 'image/jpeg', '', 1607963789, 1607963789, 1607963789, '074842ebd34159d659a68d5a552e57f6918daedf', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (41, 0, 0, '点我.png', '/storage/uploads/image/20210126/5e97675502aee45135d52425be6eba1e.png', '180', '180', 'png', 0, 24843, 'image/png', '', 1611643297, 1611643297, 1611643297, 'a39053b8f047ab2c3a9b1207774e94bc6459061c', 'public', 1);
INSERT INTO `zqs_attachment` VALUES (42, 0, 0, '矩形 2 拷贝 22.png', '/uploads/image/20211221/ac50e4892ab79cc6e8f3959cab326c41.png', '80', '80', 'png', 0, 4134, 'image/png', '', 1640097651, 1640097651, 1640097651, 'a764479b37fa1d2cb839c9e931584a0fce6dfe82', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (44, 0, 0, 'WechatIMG102761.png', '/uploads/image/20211225/cdd34a6b8594062c6070482f0534ddb1.png', '192', '192', 'png', 0, 63691, 'image/png', '', 1640367163, 1640367163, 1640367163, '939c83d7d979e29296750c4bd65c7073aa032788', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (45, 0, 0, '1028791640323545_.pic.png', '/uploads/image/20211225/7a29d51578b0bb4920f1354dd0f9f5ec.png', '881', '1124', 'png', 0, 329762, 'image/png', '', 1640367163, 1640367163, 1640367163, 'd0dd81d32826dc6ff635826c9adf1b9c5638432c', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (47, 0, 0, '小米1080x1920.png', '/uploads/image/20211225/04e38f779a3badfdfcda793e040122b1.png', '1080', '1920', 'png', 0, 879745, 'image/png', '', 1640367163, 1640367163, 1640367163, 'd2b5d68cbc35b8628b1e1e693e49104010764c31', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (48, 0, 0, 'gd.png', '/uploads/image/20211226/81060518563386d5f7ecf19c4e26bc39.png', '224', '254', 'png', 0, 22120, 'image/png', '', 1640448880, 1640448880, 1640448880, '6150f14f30d796a873651fbca4bf99c11500be4f', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (49, 0, 0, 'jy.png', '/uploads/image/20211226/610e238f977bd7e567158a061cd1c035.png', '224', '254', 'png', 0, 23149, 'image/png', '', 1640448884, 1640448884, 1640448884, '3e38d0f404200cd82e6fbe7b5b9c20052792757f', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (50, 0, 0, 'hy.png', '/uploads/image/20211226/1f04fb579222dcef98d78c040019ae8d.png', '224', '254', 'png', 0, 27960, 'image/png', '', 1640450597, 1640450597, 1640450597, '9b65aba564e38a8eb04d52dddf4cb4cde5a61b1d', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (51, 0, 0, 'WechatIMG106517.png', '/uploads/image/20220105/fd9dd815c52f872a3e3b2c6f3bb976ce.png', '975', '650', 'png', 0, 82373, 'image/png', '', 1641313785, 1641313785, 1641313785, '16c7af19bd45d123e1e71e9fcefe1fa394cee7d1', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (52, 11, 0, 'logo.png', '/uploads/image/20220105/6aecf1a6f12e6ca0fe367a025b6fc945.png', '252', '92', 'png', 0, 5095, 'image/png', '', 1641314237, 1641314237, 1641314237, 'abcc9df710b408f7debeb0ec1fcd01260ae6e349', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (53, 11, 0, '海澜之家.png', '/uploads/image/20220105/f40137b65a932eb57027e7caff6c08ea.png', '345', '200', 'png', 0, 113949, 'image/png', '', 1641314302, 1641314302, 1641314302, '1ecb31b0591e35a4b272e1ac1a5ec098cf27223b', 'aliyun', 1);
INSERT INTO `zqs_attachment` VALUES (54, 16, 0, '元旦活动头图2.png', '/uploads/image/20220105/8fab2227de1363c76c426e9101f48e2f.png', '750', '500', 'png', 0, 512845, 'image/png', '', 1641315730, 1641315730, 1641315730, '6856ed3f180661b7621c2d70949cc2e079e178aa', 'aliyun', 1);
COMMIT;

-- ----------------------------
-- Table structure for zqs_attachment_cate
-- ----------------------------
DROP TABLE IF EXISTS `zqs_attachment_cate`;
CREATE TABLE `zqs_attachment_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '父类',
  `name` varchar(20) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='附件分类';

-- ----------------------------
-- Records of zqs_attachment_cate
-- ----------------------------
BEGIN;
INSERT INTO `zqs_attachment_cate` VALUES (11, 0, '分类二', 5, 1586960732, 1586960732, 1);
COMMIT;

-- ----------------------------
-- Table structure for zqs_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `zqs_auth_group`;
CREATE TABLE `zqs_auth_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父组别',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '组名',
  `rules` text NOT NULL COMMENT '规则ID',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='分组表';

-- ----------------------------
-- Records of zqs_auth_group
-- ----------------------------
BEGIN;
INSERT INTO `zqs_auth_group` VALUES (1, 0, '超级管理员', '*', 0, 1497205035, 1);
INSERT INTO `zqs_auth_group` VALUES (2, 1, '测试2', '7,9,20,21,22,23,39,40', 1586147415, 1618077335, 1);
COMMIT;

-- ----------------------------
-- Table structure for zqs_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `zqs_auth_group_access`;
CREATE TABLE `zqs_auth_group_access` (
  `uid` int(10) unsigned NOT NULL COMMENT '会员ID',
  `group_id` int(10) unsigned NOT NULL COMMENT '级别ID',
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限分组表';

-- ----------------------------
-- Records of zqs_auth_group_access
-- ----------------------------
BEGIN;
INSERT INTO `zqs_auth_group_access` VALUES (1, 1);
INSERT INTO `zqs_auth_group_access` VALUES (4, 2);
INSERT INTO `zqs_auth_group_access` VALUES (5, 2);
COMMIT;

-- ----------------------------
-- Table structure for zqs_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `zqs_auth_rule`;
CREATE TABLE `zqs_auth_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('menu','file') NOT NULL DEFAULT 'file' COMMENT 'menu为菜单,file为权限节点',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '父ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '规则名称',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '规则名称',
  `icon` varchar(50) NOT NULL DEFAULT '' COMMENT '图标',
  `condition` varchar(255) NOT NULL DEFAULT '' COMMENT '条件',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `ismenu` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否为菜单',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`) USING BTREE,
  KEY `weigh` (`weigh`) USING BTREE,
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COMMENT='节点表';

-- ----------------------------
-- Records of zqs_auth_rule
-- ----------------------------
BEGIN;
INSERT INTO `zqs_auth_rule` VALUES (1, 'file', 0, 'auth', '权限', 'layui-icon-user', '', '', 1, 1586088731, 1586706795, -99, 1);
INSERT INTO `zqs_auth_rule` VALUES (2, 'file', 1, '/auth/rule/index', '菜单节点', '', '', '', 1, 1586088759, 1586327772, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (3, 'file', 1, '/auth/adminuser/index', '管理员', '', '', '', 1, 1586089478, 1586089509, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (4, 'file', 1, '/auth/group/index', '角色组', '', '', '', 1, 1586089655, 1586089655, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (5, 'file', 2, '/auth/rule/index', '查看', '', '', '', 0, 1586090145, 1586090280, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (6, 'file', 2, '/auth/rule/add', '添加', '', '', '', 0, 1586090312, 1586090312, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (7, 'file', 0, 'config', '设置', 'layui-icon-set', '', '', 1, 1586521035, 1586630637, -98, 1);
INSERT INTO `zqs_auth_rule` VALUES (8, 'file', 7, '/config/system', '系统设置', '', '', '', 1, 1586528420, 1586584859, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (9, 'file', 7, '/config/index', '配置管理', '', '', '', 1, 1586528441, 1586707997, -1, 1);
INSERT INTO `zqs_auth_rule` VALUES (10, 'file', 2, '/auth/rule/edit', '编辑', '', '', '', 0, 1586614418, 1586614418, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (11, 'file', 2, '/auth/rule/delete', '删除', '', '', '', 0, 1586614439, 1586614439, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (12, 'file', 3, '/auth/adminuser/index', '查看', '', '', '', 0, 1586614496, 1586614496, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (13, 'file', 3, '/auth/adminuser/add', '添加', '', '', '', 0, 1586614511, 1586614531, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (14, 'file', 3, '/auth/adminuser/edit', '编辑', '', '', '', 0, 1586614545, 1586614545, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (15, 'file', 3, '/auth/adminuser/delete', '删除', '', '', '', 0, 1586614563, 1586614563, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (16, 'file', 4, 'auth/group/index', '查看', '', '', '', 0, 1586614585, 1586614585, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (17, 'file', 4, 'auth/group/add', '添加', '', '', '', 0, 1586614599, 1586614599, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (18, 'file', 4, 'auth/group/edit', '编辑', '', '', '', 0, 1586614612, 1586614612, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (19, 'file', 4, 'auth/group/delete', '删除', '', '', '', 0, 1586614629, 1586614629, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (20, 'file', 9, '/config/index', '查看', '', '', '', 0, 1586617847, 1586617847, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (21, 'file', 9, '/config/add', '添加', '', '', '', 0, 1586617911, 1586617911, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (22, 'file', 9, '/config/edit', '编辑', '', '', '', 0, 1586617927, 1586617927, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (23, 'file', 9, '/config/delete', '删除', '', '', '', 0, 1586617943, 1586617943, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (24, 'file', 8, '/config/system_save', '保存', '', '', '', 0, 1586682903, 1586682903, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (25, 'file', 0, 'cms', '内容', 'layui-icon-read', '', '', 1, 1586706681, 1586706681, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (26, 'file', 25, '/cms/article/index', '文章列表', '', '', '', 1, 1586707123, 1587020245, 1, 1);
INSERT INTO `zqs_auth_rule` VALUES (27, 'file', 25, '/cms/cate/index', '文章分类', '', '', '', 1, 1586707151, 1586707151, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (28, 'file', 25, '/cms/attach/index', '附件管理', '', '', '', 1, 1586707187, 1586707951, -1, 1);
INSERT INTO `zqs_auth_rule` VALUES (29, 'file', 28, '/cms/attach/index', '查看', '', '', '', 0, 1586786084, 1586786084, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (30, 'file', 28, '/cms/attach/delete', '删除', '', '', '', 0, 1586786103, 1586786103, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (31, 'file', 26, '/cms/article/index', '查看', '', '', '', 0, 1586786349, 1587020061, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (32, 'file', 26, '/cms/article/add', '添加', '', '', '', 0, 1587019579, 1587019641, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (33, 'file', 26, '/cms/article/edit', '编辑', '', '', '', 0, 1587019678, 1587019683, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (34, 'file', 26, '/cms/article/delete', '删除', '', '', '', 0, 1587019696, 1587019696, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (35, 'file', 27, '/cms/cate/index', '查看', '', '', '', 0, 1587019729, 1587019729, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (36, 'file', 27, '/cms/cate/add', '添加', '', '', '', 0, 1587019764, 1587019764, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (37, 'file', 27, '/cms/cate/edit', '编辑', '', '', '', 0, 1587019777, 1587019777, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (38, 'file', 27, '/cms/cate/delete', '删除', '', '', '', 0, 1587019795, 1587019795, 0, 1);
INSERT INTO `zqs_auth_rule` VALUES (39, 'file', 0, 'test', '测试', 'layui-icon-auz', '', '', 1, 1607876187, 1607876187, 99, 1);
INSERT INTO `zqs_auth_rule` VALUES (40, 'file', 39, '.test/index', '列表', '', '', '', 1, 1607876228, 1607876228, 0, 1);
COMMIT;

-- ----------------------------
-- Table structure for zqs_config
-- ----------------------------
DROP TABLE IF EXISTS `zqs_config`;
CREATE TABLE `zqs_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置ID',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '配置名称 字段名',
  `type` varchar(30) NOT NULL DEFAULT '' COMMENT '配置类型 input/select/array...',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '配置说明',
  `group` varchar(30) NOT NULL DEFAULT '0' COMMENT '配置分组',
  `options` varchar(255) DEFAULT NULL COMMENT '配置项',
  `value` text COMMENT '配置值',
  `sort` smallint(3) NOT NULL DEFAULT '0' COMMENT '排序',
  `remark` varchar(100) DEFAULT NULL COMMENT '配置说明',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zqs_config
-- ----------------------------
BEGIN;
INSERT INTO `zqs_config` VALUES (1, 'config_group', 'array', '配置分组', 'system', '', 'base:基础\nsystem:系统\nupload:上传\ndevelop:开发\ndatabase:数据库', 0, '', 0, 1586605081, 1);
INSERT INTO `zqs_config` VALUES (2, 'form_item_type', 'array', '配置类型', 'system', '', 'input:单行文本\ntextarea:多行文本\nswitch:开关\nselect:下拉菜单\nselect2:下拉菜单多选\nimage:单图上传\nicon:图标\nradio:单选\narray:数组\ntime:时间\ndate:日期\ndate-range:日期范围', 0, '', 0, 1635864069, 1);
INSERT INTO `zqs_config` VALUES (9, 'a', 'input', '单行文本', 'base', '', 'aaaa', 0, '', 1586623301, 1635864470, 1);
INSERT INTO `zqs_config` VALUES (10, 'b', 'textarea', '多行文本', 'base', '', 'bbbb123', 0, '', 1586623804, 1635864470, 1);
INSERT INTO `zqs_config` VALUES (11, 'c', 'switch', '开关', 'base', '', '1', 0, '', 1586675857, 1635864470, 1);
INSERT INTO `zqs_config` VALUES (12, 'd', 'select', '下拉菜单', 'base', 'a:是\nb:否\nc:中立\nd:逃', 'b', 0, '', 1586676357, 1635864470, 1);
INSERT INTO `zqs_config` VALUES (13, 'e', 'image', '单图上传', 'base', '', '24', 0, '', 1586676780, 1635864470, 1);
INSERT INTO `zqs_config` VALUES (14, 'f', 'icon', '图标选择', 'base', '', 'layui-icon-login-wechat', 0, '', 1586677153, 1635864470, 1);
INSERT INTO `zqs_config` VALUES (15, 'g', 'radio', '单选', 'base', '1:x\n2:y', '1', 0, '', 1586677192, 1635864470, 1);
INSERT INTO `zqs_config` VALUES (16, 'h', 'array', '数组', 'base', '', '1:1\n2:2', 0, '', 1586677314, 1635864470, 1);
INSERT INTO `zqs_config` VALUES (17, 'i', 'select2', '下拉多选', 'base', 'a:1\nb:2\n3:3\n4:4', 'a,b,3,4', 0, '', 1586677489, 1635864470, 1);
INSERT INTO `zqs_config` VALUES (18, 'upload_disks', 'select', '上传存储方式', 'upload', 'public:本地服务器\naliyun:阿里云\nqiniu:七牛云\nqcloud:腾讯云', 'aliyun', 0, '', 1607952460, 1611644361, 1);
INSERT INTO `zqs_config` VALUES (19, 'upload_aliyun_accessId', 'input', '阿里云(accessId)', 'upload', '', 'LTAI4GBhUCkXvdw898G1aJiu', 0, '', 1607956116, 1611644361, 1);
INSERT INTO `zqs_config` VALUES (20, 'upload_aliyun_accessSecret', 'input', '阿里云(accessSecret)', 'upload', '', '2YoRTHJnhMVJDSP14D6B5R8WW8cAqY', 0, '', 1607956218, 1611644361, 1);
INSERT INTO `zqs_config` VALUES (21, 'upload_aliyun_bucket', 'input', '阿里云(bucket)', 'upload', '', 'yqyx', 0, '', 1607956262, 1611644361, 1);
INSERT INTO `zqs_config` VALUES (22, 'upload_aliyun_endpoint', 'input', '阿里云(endpoint)', 'upload', '', 'oss-cn-beijing.aliyuncs.com', 0, '', 1607956313, 1611644361, 1);
INSERT INTO `zqs_config` VALUES (23, 'upload_aliyun_url', 'input', '阿里云(url)', 'upload', '', 'https://yqyx.oss-cn-beijing.aliyuncs.com', 0, '', 1607956344, 1611644361, 1);
INSERT INTO `zqs_config` VALUES (24, 'upload_qiniu_accessKey', 'input', '七牛(accessKey)', 'upload', '', 'a', 0, '', 1607956393, 1611644361, 1);
INSERT INTO `zqs_config` VALUES (25, 'upload_qiniu_secretKey', 'input', '七牛(secretKey)', 'upload', '', 'b', 0, '', 1607956418, 1611644361, 1);
INSERT INTO `zqs_config` VALUES (26, 'upload_qiniu_bucket', 'input', '七牛(bucket)', 'upload', '', 'c', 0, '', 1607956456, 1611644361, 1);
INSERT INTO `zqs_config` VALUES (27, 'upload_qiniu_url', 'input', '七牛(url)', 'upload', '', 'd', 0, '', 1607956483, 1611644361, 1);
INSERT INTO `zqs_config` VALUES (28, 'time', 'time', '时间', 'base', '', '', 0, '', 1635862114, 1635864470, 1);
INSERT INTO `zqs_config` VALUES (29, 'date1', 'date', '选择日期', 'base', '', '', 0, '', 1635862335, 1635864470, 1);
INSERT INTO `zqs_config` VALUES (30, 'date_range', 'date-range', '日期范围', 'base', '', '2021-11-02 - 2021-12-02', 0, '', 1635864095, 1635864470, 1);
COMMIT;

-- ----------------------------
-- Table structure for zqs_goods
-- ----------------------------
DROP TABLE IF EXISTS `zqs_goods`;
CREATE TABLE `zqs_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL COMMENT '产品标题',
  `intro` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '简介',
  `cover_img` int(10) NOT NULL DEFAULT '0' COMMENT '封面图片',
  `cover_video` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '封面视频',
  `imgs` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '产品图列表',
  `original_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '原价',
  `cost_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '成本价',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '销售价',
  `desc` text COMMENT '详情',
  `stocks` int(10) NOT NULL DEFAULT '0' COMMENT '库存',
  `sales` int(10) NOT NULL DEFAULT '0' COMMENT '销量',
  `spec` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '规格',
  `commission` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '直推佣金、分享赚佣金 商城有效',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0：商城 1：拼团 2：1元抢',
  `pintuan_num` int(11) NOT NULL DEFAULT '0' COMMENT 'type=1时生效  成团人数',
  `pintuan_win_num` int(11) NOT NULL DEFAULT '0' COMMENT '拼团中奖人数',
  `pintuan_reward` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '拼团未中奖的奖励金额',
  `yiyuan_num` int(11) NOT NULL DEFAULT '0' COMMENT '1元抢成团人数',
  `yiyuan_reward_uid` int(11) NOT NULL DEFAULT '0' COMMENT '指定用户id中奖',
  `sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序，越大越靠前',
  `show_index` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否首页展示',
  `share_count` int(10) NOT NULL DEFAULT '0' COMMENT '分享次数',
  `is_vip` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否vip产品，只有vip才能购买 ',
  `vip_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'vip价格',
  `is_fenxiao` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否分销 极差佣金',
  `is_area` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否区域代理分销',
  `is_vip_be` tinyint(1) NOT NULL DEFAULT '0' COMMENT '购买成功是否成为vip',
  `share_title` varchar(255) DEFAULT NULL COMMENT '分享标题',
  `share_img` int(10) NOT NULL DEFAULT '0' COMMENT '分享图片',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='产品列表';

-- ----------------------------
-- Records of zqs_goods
-- ----------------------------
BEGIN;
INSERT INTO `zqs_goods` VALUES (1, '商城产品1', '上周五，指数全天震荡走弱，尾盘有所回升，个股普跌，市场行情表现低迷，资金观望情绪浓厚', 1, 'https://yqyx.oss-cn-beijing.aliyuncs.com/static/video/jieshao.mp4', '4,5,6', 100.00, 50.00, 80.00, '<p style=\"box-sizing: inherit; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); margin: 10px 0px 25px; padding: 0px; border: 0px; font-size: 18px; vertical-align: baseline; background: #ffffff; color: #222222; font-family: Pingfang-SC, \'Microsoft YaHei\', sans-serif; text-align: justify;\"><img src=\"https://yqyx.oss-cn-beijing.aliyuncs.com/uploads/image/20210127/9b99e10d4d9dab91578bdb6f99ac6769.jpg\" alt=\"\" width=\"1080\" height=\"864\" /><img src=\"https://yqyx.oss-cn-beijing.aliyuncs.com/uploads/image/20210115/b6c03a2e903a5938178264bc96f48f9f.png\" alt=\"\" width=\"1080\" height=\"865\" />其次，我们预计年末经济数据会继续超预期，海外第二轮全球同步宽松带来充裕的外部增量流动性，整体信用环境依旧稳健，并且下周中央经济工作会议预计不会有超预期变化，社融、流动性、信用风险和宏观经济政策这几个短期被扰动的预期将很快会重聚共识。</p>\n<p style=\"box-sizing: inherit; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); margin: 10px 0px 25px; padding: 0px; border: 0px; font-size: 18px; vertical-align: baseline; background: #ffffff; color: #222222; font-family: Pingfang-SC, \'Microsoft YaHei\', sans-serif; text-align: justify;\">最后，临近年末机构产品排名竞争依旧焦灼，短期内博弈或将加剧，白酒、医药、新能源、消费电子中机构重仓股抱团进一步强化，非抱团股可能出现一定波动，但博弈的心态和交易行为在跨年后预计将趋于缓和。</p>\n<p style=\"box-sizing: inherit; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); margin: 10px 0px 25px; padding: 0px; border: 0px; font-size: 18px; vertical-align: baseline; background: #ffffff; color: #222222; font-family: Pingfang-SC, \'Microsoft YaHei\', sans-serif; text-align: justify;\">A股仍处于跨年轮动慢涨期，基本面预期逐步改善抬升市场底线，增量资金缓慢入场，短期因市场波动被扰乱的预期预计会重聚共识，因此建议坚守顺周期主线跨年，以应对年末博弈心态和行为加剧的市场。</p>\n<p style=\"box-sizing: inherit; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); margin: 10px 0px 25px; padding: 0px; border: 0px; font-size: 18px; vertical-align: baseline; background: #ffffff; color: #222222; font-family: Pingfang-SC, \'Microsoft YaHei\', sans-serif; text-align: justify;\">配置上，顺周期主线，<strong style=\"box-sizing: inherit; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;\">继续推荐基本金属、能源金属、基础化工以及家电、汽车、白酒、家居、酒店等可选消费行业</strong>。同时关注年末两条轮动副线，一是今年相对滞涨但景气向好的品种，包括出口产业链中的电子、汽车零部件和轻工，以及优质的地产蓝筹；二是年底机构博弈驱动的医药、新能源和食品饮料板块的快速轮动机会。</p>\n<p style=\"box-sizing: inherit; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); margin: 10px 0px 25px; padding: 0px; border: 0px; font-size: 18px; vertical-align: baseline; background: #ffffff; color: #222222; font-family: Pingfang-SC, \'Microsoft YaHei\', sans-serif; text-align: justify;\"><strong style=\"box-sizing: inherit; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); margin: 0px; padding: 0px; border: 0px; vertical-align: baseline; background: transparent;\">兴业证券：需求、供给侧改革双升级 复苏行情有望超预期</strong></p>', 88, 12, 'a,b', 10.00, 0, 0, 0, 0.00, 0, 0, 99, 1, 0, 0, 70.00, 1, 0, 1, '啊啊啊啊啊中', 13, 1607864337, 1611732265, 1);
INSERT INTO `zqs_goods` VALUES (2, '测试产品2', '测试产品2测试产品2测试产品2测试产品2测试产品2', 2, 'https://yqyx.oss-cn-beijing.aliyuncs.com/static/video/1609841063203160.mp4', '5,4,6', 200.00, 58.00, 158.00, '', 100, 0, '', 36.00, 0, 0, 0, 0.00, 0, 0, 1, 1, 0, 1, 120.00, 1, 1, 0, NULL, 0, 1607870837, 1611634183, 1);
INSERT INTO `zqs_goods` VALUES (3, '拼团商品1', '拼团商品1拼团商品1拼团商品1拼团商品1拼团商品1拼团商品1', 47, 'https://yqyx.oss-cn-beijing.aliyuncs.com/uploads/265334169-1-192.mp4', '42,41', 100.00, 10.00, 50.00, '<p><a class=\"f-w-b c-ef9524 b-c-222\" style=\"box-sizing: inherit; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); margin: 0px; padding: 0px; vertical-align: baseline; background: #ffffff; text-decoration-line: none; color: #ef9524; font-weight: bold; border-color: #222222; font-family: Pingfang-SC, \'Microsoft YaHei\', sans-serif; font-size: 27px;\" href=\"https://www.cls.cn/detail/639400\" target=\"_blank\" rel=\"noopener noreferrer\">十大券商策略：A股仍处于跨年轮动慢涨期 坚守顺周期主线跨年</a><a class=\"f-w-b c-ef9524 b-c-222\" style=\"box-sizing: inherit; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); margin: 0px; padding: 0px; vertical-align: baseline; background: #ffffff; text-decoration-line: none; color: #ef9524; font-weight: bold; border-color: #222222; font-family: Pingfang-SC, \'Microsoft YaHei\', sans-serif; font-size: 27px;\" href=\"https://www.cls.cn/detail/639400\" target=\"_blank\" rel=\"noopener noreferrer\">十大券商策略：A股仍处于跨年轮动慢涨期 坚守顺周期主线跨年</a></p>', 92, 8, '红的,黄的', 0.00, 1, 5, 1, 5.90, 0, 0, 100, 1, 0, 0, 0.00, 0, 0, 0, '', 0, 1607875326, 1640624180, 1);
INSERT INTO `zqs_goods` VALUES (4, '拼团2', '拼团2拼团2拼团2', 9, '', '11,10', 100.00, 30.00, 56.00, '', 99999, 10059, '', 0.00, 1, 5, 1, 3.60, 0, 0, 0, 1, 0, 0, 0.00, 0, 0, 0, '', 0, 1609257800, 1622915301, 1);
INSERT INTO `zqs_goods` VALUES (5, '拼团产品33333333333', '拼团产品33333333333', 12, '', '13,13', 500.00, 30.00, 100.00, '<p>123456789789789798</p>', 100, 0, '', 0.00, 1, 3, 2, 8.00, 0, 0, 0, 1, 0, 0, 0.00, 0, 0, 0, NULL, 0, 1610650415, 1610650440, 1);
INSERT INTO `zqs_goods` VALUES (6, '一元抢111111不指定人中奖', '一元抢111111', 12, '', '13,12', 1000.00, 800.00, 1.00, '<p>哈哈<span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, \'Segoe UI\', Roboto, Oxygen, Ubuntu, Cantarell, \'Open Sans\', \'Helvetica Neue\', sans-serif;\">😄</span></p>', 93, 7, '黄的,蓝色的', 0.00, 2, 0, 0, 0.00, 2, 0, 0, 0, 0, 0, 0.00, 0, 0, 0, '啊啊啊啊啊啊啊啊啊啊', 12, 1610977527, 1611736365, 1);
INSERT INTO `zqs_goods` VALUES (7, '一元抢商品22222', '一元抢商品22222', 3, '', '5,5', 888.00, 555.00, 1.00, '', 999, 1, '', 0.00, 2, 0, 0, 0.00, 10, 100001, 0, 0, 0, 0, 0.00, 0, 0, 0, '啊啊啊，分享啊', 12, 1610986605, 1611680652, 1);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
