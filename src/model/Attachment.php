<?php

namespace zqs\admin\model;

use think\Model;

class Attachment extends Model
{
    public function getUrlAttr($value,$data)
    {
        if ($data['storage']!='public'){
            return config_db('upload_'.$data['storage'].'_url').$value;
        }else{
            return config('app.app_host').$value;
        }
        return $value;
    }
   

}
