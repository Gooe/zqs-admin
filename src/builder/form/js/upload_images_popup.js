$('#upload_imgs_popup_{$name}').click(function (){
    //还可以选择的数量=总数-已经选择过的数量
    var max = {$max};
    var has_num = $('#upload_imgs_popup_view_{$name} .img-list').length;

    var index = admin.popup({
        title:'选择图片'
        ,type:2
        ,content:'/{:get_admin_name()}/images/index?field_name={$name}&max={$max}'
        ,area:['{$popup_width|default=\'70%\'}','{$popup_height|default=\'60%\'}']
        ,shadeClose:false
    })
    window.callbackChooseImages = function (ids,urls) {
        // console.log(ids,urls,has_num)
        //如果是单图
        if (max==1){
            layui.$('.img-tips').hide();
            layui.$('#upload_imgs_popup_view_{$name}').removeClass('layui-hide').find('img').attr('src', urls);
            layui.$('#upload_imgs_popup_view_{$name}').find('input').val(ids);
        }else{
            if(has_num + ids.length > max){
                layer.msg('最多还可以选择'+Math.max(max-has_num,0)+'张图片');
                return false;
            }
            var html = '';
            $.each(urls,function (k,v){
                html += '<div class="img-list"><img src="'+v+'" data-v="'+ids[k]+'" ><i class="layui-icon layui-icon-close del_imgs_{$name}" ></i></div>';
            })
            layui.$('#upload_imgs_popup_{$name}').before(html);

            //添加值
            var v = layui.$('#upload_imgs_popup_view_{$name}').find('input').val();
            if (v && v!=='0'){
                ids = v.split(',').concat(ids)
            }
            layui.$('#upload_imgs_popup_view_{$name}').find('input').val(ids);
        }
        //要判断最多可选择几张图片，要算上已经先择过的
        layer.close(index)
    };
});
//删除图片
$('#upload_imgs_popup_view_{$name}').on('click','.del_imgs_{$name}',function (event){
    var v = layui.$('#upload_imgs_popup_view_{$name}').find('input').val();
    var arr = v.split(',');
    if (arr.length==1){
        new_v = '';
    }else if(arr.length>1){
        var index_v = $(this).prev().data('v');
        var index = arr.indexOf(index_v.toString());
        arr.splice(index,1);
        new_v = arr.toString();
    }else {
        new_v = '';
    }
    layui.$('#upload_imgs_popup_view_{$name}').find('input').val(new_v);
    layui.$(this).parent().remove();

    event.stopPropagation();
});
