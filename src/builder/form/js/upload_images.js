//拖拽上传,多图
upload.render({
    elem: '#upload_imgs_{$name}'
    ,url: '{:url(\'/\'.get_admin_path().'/upload/image')}' //改成您自己的上传接口
    ,accept:'images'
    ,field:'image'
    ,acceptMime: 'image/*'
    ,multiple: true
    ,done: function(res){
        if (res.code==1) {
            layui.$('#upload_imgs_view_{$name}').removeClass('layui-hide').append('<div class="preview_imgs_item">\n' +
                '                    <img src="'+res.data.url+'" data-v="'+res.data.id+'">\n' +
                '                    <i class="layui-icon layui-icon-close-fill del_imgs_{$name}" style="font-size: 20px; color: #ff0000;" ></i>\n' +
                '                </div>');

            var v = layui.$('#upload_imgs_view_{$name}').find('input').val();
            if (v){
                v += ',' + res.data.id;
            }else {
                v = res.data.id;
            }
            layui.$('#upload_imgs_view_{$name}').find('input').val(v);
            layer.msg(res.msg,{icon:1});
        }else{
            layer.msg(res.msg,{icon:2});
        }
    }
});


$('#upload_imgs_{$name}').on('click','.del_imgs_{$name}',function (event){
    var v = layui.$('#upload_imgs_view_{$name}').find('input').val();
    var arr = v.split(',');
    if (arr.length==1){
        new_v = '';
    }else if(arr.length>1){
        var index_v = $(this).prev().data('v');
        var index = arr.indexOf(index_v.toString());
        arr.splice(index,1);
        new_v = arr.toString();
    }else {
        new_v = '';
    }
    layui.$('#upload_imgs_view_{$name}').find('input').val(new_v);
    layui.$(this).parent().remove();

    event.stopPropagation();
});